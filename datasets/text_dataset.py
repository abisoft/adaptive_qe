import pandas as pd

from torch.utils import data

class QualityEstimationDataset(data.Dataset):
    def __init__(self, file_path, split, num_instances=-1):
        lang_pair = file_path.split("/")[-3]
        task = file_path.split("/")[-2]
        self.dataset_name = split+" quality estimation dataset for lang_pair:{}, user:{}".format(lang_pair,task)
        self.task_name = lang_pair+"-"+task

        self.data = pd.read_csv(file_path, header=None, sep=',', names=['labels', 'source', 'translation'],index_col=False)
        self.data.dropna(inplace=True)

        self.data['labels'] = self.data['labels'].astype('int32')
        self.n_classes = 1
        if (num_instances != -1):
            self.data = self.data.sample(n=num_instances, replace=True,random_state=42)

    def __len__(self):
        return len(self.data)

    def __getitem__(self, index):
        source = self.data['source'].iloc[index]
        translation = self.data['translation'].iloc[index]
        label = self.data['labels'].iloc[index]
        return (source, translation), label


