import torch
from datasets.text_dataset import QualityEstimationDataset


def batch_encode(batch):
    text, labels = [], []
    for txt, lbl in batch:
        text.append(txt)
        labels.append(lbl)
    return text, labels

def get_dataset(base_path, dataset, dataset_id,num_train_instances=-1):
    train_path = base_path+'/data/'+dataset+'/'+dataset_id+'/train.csv'
    dev_path = base_path+'/data/'+dataset+'/'+dataset_id+'/dev.csv'
    test_path = base_path+'/data/'+dataset+'/'+dataset_id+'/test.csv'

    train_dataset = QualityEstimationDataset(train_path, 'train', num_instances=num_train_instances)
    dev_dataset = QualityEstimationDataset(dev_path, 'dev')
    test_dataset = QualityEstimationDataset(test_path, 'test')

    return train_dataset, dev_dataset, test_dataset


def remove_return_sym(str):
    return str.split('\n')[0]

def get_max_len(text_list):
    return max([len(x) for x in text_list])


def glove_vectorize(text, glove, dim=300):
    max_len = get_max_len(text)
    lengths = []
    vec = torch.ones((len(text), max_len, dim))
    for i, sent in enumerate(text):
        sent_emb = glove.get_vecs_by_tokens(sent, lower_case_backup=True)
        vec[i, :len(sent_emb)] = sent_emb
        lengths.append(len(sent))
    lengths = torch.tensor(lengths)
    return vec, lengths