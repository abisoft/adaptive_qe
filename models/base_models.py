import random

import torch
from torch import nn

from transformers import AlbertModel, AlbertTokenizer, BertTokenizer, BertModel
from transformers import DistilBertModel, DistilBertTokenizer


from models.utils import WGF_step

import models.utils



class Transformer(nn.Module):
    def __init__(self, model_name, max_length, device):
        super(Transformer, self).__init__()
        self.max_length = max_length
        self.device = device
        if model_name == 'albert':
            self.tokenizer = AlbertTokenizer.from_pretrained('albert-base-v2')
            self.encoder = AlbertModel.from_pretrained('albert-base-v2')
        elif model_name == 'bert':
            self.tokenizer = BertTokenizer.from_pretrained('bert-base-uncased')
            self.encoder = BertModel.from_pretrained('bert-base-uncased')
        elif model_name == 'distil-multi-bert':
            self.tokenizer = DistilBertTokenizer.from_pretrained('distilbert-base-multilingual-cased')
            self.encoder = DistilBertModel.from_pretrained('distilbert-base-multilingual-cased')
        else:
            raise NotImplementedError

        self.to(self.device)

    def encode_text(self, text):
        encode_result = self.tokenizer.batch_encode_plus(text, return_token_type_ids=False, max_length=self.max_length,
                                                         truncation=True, padding='max_length', return_tensors='pt')
        for key in encode_result:
            encode_result[key] = encode_result[key].to(self.device)
        return encode_result

    def forward(self, inputs):
        out1 = self.encoder(inputs['input_ids'], attention_mask=inputs['attention_mask'])
        out = out1[0][:,0]
        return out




class Linear(nn.Module):

    def __init__(self, in_dim, out_dim,num_particles, device, lmbd):
        super(Linear, self).__init__()
        self.num_particles = num_particles
        self.lmbd = lmbd
        self.linear = models.utils.InferenceNet(hidden_sizes=[], input_size=in_dim, output_size=out_dim)
        self.w_shape = self.linear.get_weight_shape()
        theta = []
        self.device = torch.device(device)
        for _ in range(num_particles):
            theta_flatten = []
            for key in self.w_shape.keys():
                if isinstance(self.w_shape[key], tuple):
                    theta_temp = torch.empty(self.w_shape[key], device=device)
                    torch.nn.init.xavier_normal_(tensor=theta_temp)
                else:
                    theta_temp = torch.zeros(self.w_shape[key], device=device)
                theta_flatten.append(torch.flatten(theta_temp, start_dim=0, end_dim=-1))
            theta.append(torch.cat(theta_flatten))

        self.theta = nn.Parameter(torch.stack(theta), requires_grad=True)

        self.alpha_lr = nn.Parameter(1e-3 * torch.ones(self.theta.shape, requires_grad=True, device=device))

        self.loss_fn = nn.BCELoss()

        self.to(self.device)

    def forward(self, input, theta, labels):
        out, loss = self.get_prediction(input, theta, labels)
        return out, loss


    def get_prediction(self, inputs, theta, labels):
        for particle_id in range(self.num_particles):
            w = models.utils.get_weights_target_net(w_generated=theta, row_id=particle_id,
                                                    w_target_shape=self.w_shape)
            y_pred_ = self.linear(inputs, w=w)
            loss = self.loss_fn(y_pred_.squeeze(), labels.type_as(y_pred_).squeeze())

            if particle_id == 0:
                y_pred_v = y_pred_
                loss_sum = loss
            else:
                y_pred_v = y_pred_v + y_pred_
                loss_sum = loss_sum + loss

        return y_pred_v/self.num_particles, loss_sum/self.num_particles


    def get_prediction_update(self, x_t, y_t):
        d_NLL = []
        loss_sum = 0
        y_pred_v = 0
        for particle_id in range(self.num_particles):
            w = models.utils.get_weights_target_net(w_generated=self.theta, row_id=particle_id,
                                                    w_target_shape=self.w_shape)
            y_pred_t = self.linear(x_t, w=w)
            loss_NLL = self.loss_fn(y_pred_t.squeeze(), y_t)

            NLL_grads = torch.autograd.grad(
                outputs=loss_NLL,
                inputs=w.values(),
                retain_graph=True
            )
            if particle_id == 0:
                loss_sum = loss_NLL
                y_pred_v = y_pred_t
            else:
                loss_sum = loss_sum + loss_NLL
                y_pred_v = y_pred_v + y_pred_t
            NLL_gradients = dict(zip(w.keys(), NLL_grads))
            NLL_gradients_tensor = models.utils.dict2tensor(dict_obj=NLL_gradients)
            d_NLL.append(NLL_gradients_tensor)
        d_NLL = torch.stack(d_NLL)
        phi = WGF_step(self.theta, d_NLL)

        if self.theta.grad is not None:
            self.theta.grad = self.theta.grad + phi.detach()
        else:
            self.theta.grad = phi.detach()

        return y_pred_v/self.num_particles, loss_sum

class ReplayMemory:
    def __init__(self, write_prob, tuple_size):
        self.buffer = []
        self.write_prob = write_prob
        self.tuple_size = tuple_size

    def write(self, input_tuple):
        if random.random() < self.write_prob:
            self.buffer.append(input_tuple)

    def read(self):
        return random.choice(self.buffer)

    def write_batch(self, *elements):
        element_list = []
        for e in elements:
            if isinstance(e, torch.Tensor):
                element_list.append(e.tolist())
            else:
                element_list.append(e)
        for write_tuple in zip(*element_list):
            self.write(write_tuple)

    def read_batch(self, batch_size):
        contents = [[] for _ in range(self.tuple_size)]
        for _ in range(batch_size):
            read_tuple = self.read()
            for i in range(len(read_tuple)):
                contents[i].append(read_tuple[i])
        return tuple(contents)

    def len(self):
        return len(self.buffer)

    def reset_memory(self):
        self.buffer = []
