import torch
import torch.nn as nn
import torch.nn.functional as F
import numpy as np
from scipy.spatial.distance import pdist, squareform
from sklearn import metrics

import collections



def calculate_metrics(predictions, labels):
    accuracy = calculate_accuracy(predictions,labels)
    precision = metrics.precision_score(labels, predictions, average='weighted')
    recall = metrics.recall_score(labels, predictions, average='weighted')
    f1 = (2.0*precision*recall)/(precision+recall)
    return accuracy, precision, recall, f1

def calculate_accuracy(predictions, labels):
    predictions = np.array(predictions)
    labels = np.array(labels)
    accuracy = metrics.accuracy_score(labels, predictions)
    return accuracy

def make_prediction(output):
    return output.round().squeeze()

def identity(x):
    return x


class InferenceNet(nn.Module):
    def __init__(self,
            hidden_sizes,
            output_size,
            input_size,
            hidden_activation=F.relu,
            output_activation=torch.sigmoid,prior_sigma=1,opt=None):
        super(InferenceNet, self).__init__()

        self.prior_sigma = prior_sigma
        self.input_size = input_size
        self.output_size = output_size
        self.hidden_sizes = hidden_sizes
        self.hidden_activation = hidden_activation
        self.output_activation = output_activation
        self.opt= opt

    def get_weight_shape(self):
        weight_shape = collections.OrderedDict()
        num_hidden_units = [self.input_size]
        num_hidden_units.extend(self.hidden_sizes)
        num_hidden_units.append(self.output_size)

        for i in range(len(self.hidden_sizes)+1):
            weight_shape['w{0:d}'.format(i + 1)] = (num_hidden_units[i + 1], num_hidden_units[i])
            weight_shape['b{0:d}'.format(i + 1)] = num_hidden_units[i + 1]

        return weight_shape

    def initialise_weights(self):
        w = {}
        w_shape = self.get_weight_shape()
        for key in w_shape.keys():
            if ('w' in key):
                w[key] = torch.empty(w_shape[key], device=torch.device('cuda'))
                torch.nn.init.xavier_normal_(tensor=w[key])
                w[key].requires_grad_()
            if 'b' in key:
                w[key] = torch.zeros(w_shape[key], requires_grad=True, device=torch.device('cuda'))
        return w

    def forward(self, input, w):
        out = input
        for i in range(len(self.hidden_sizes) + 1):
            out = torch.nn.functional.linear(
                input=out,
                weight=w['w{0:d}'.format(i + 1)],
                bias=w['b{0:d}'.format(i + 1)]
            )
            if (i < (len(self.hidden_sizes))):
                out = self.hidden_activation(out)
            else:
                out = self.output_activation(out)
        return out

    def softrelu(self, x):
        return torch.log1p(torch.exp(x))



def get_weights_target_net(w_generated, row_id, w_target_shape):
    w = {}
    temp = 0
    for key in w_target_shape.keys():
        w_temp = w_generated[row_id, temp:(temp + np.prod(w_target_shape[key]))]
        if 'b' in key:
            w[key] = w_temp
        else:
            w[key] = w_temp.view(w_target_shape[key])
        temp += np.prod(w_target_shape[key])
    return w

def dict2tensor(dict_obj):
    d2tensor = []
    for key in dict_obj.keys():
        tensor_temp = torch.flatten(dict_obj[key], start_dim=0, end_dim=-1)
        d2tensor.append(tensor_temp)
    d2tensor = torch.cat(d2tensor)
    return d2tensor


def get_kernel(particle_tensor,num_particles):
    '''
    Compute the RBF kernel for the input particles
    Input: particles = tensor of shape (N, M)
    Output: kernel_matrix = tensor of shape (N, N)
    '''
    pairwise_d_matrix = get_pairwise_distance_matrix(particle_tensor)

    median_dist = torch.median(pairwise_d_matrix)

    h = median_dist / (np.log(num_particles)+1e-10)

    kernel_matrix = torch.exp(-pairwise_d_matrix / (h+1e-10))
    kernel_sum = torch.sum(input=kernel_matrix, dim=1, keepdim=True)
    grad_kernel = -torch.matmul(kernel_matrix, particle_tensor)
    grad_kernel += particle_tensor * kernel_sum
    grad_kernel /= (h+1e-10)
    return kernel_matrix, grad_kernel, h

def get_pairwise_distance_matrix(particle_tensor):
    '''
    Input: tensors of particles
    Output: matrix of pairwise distances
    '''
    num_particles = particle_tensor.shape[0]
    euclidean_dists = torch.nn.functional.pdist(input=particle_tensor, p=2) # shape of (N)

    # initialize matrix of pairwise distances as a N x N matrix
    pairwise_d_matrix = torch.zeros((num_particles, num_particles), device=torch.device('cuda'))

    # assign upper-triangle part
    triu_indices = torch.triu_indices(row=num_particles, col=num_particles, offset=1)
    pairwise_d_matrix[triu_indices[0], triu_indices[1]] = euclidean_dists

    # assign lower-triangle part
    pairwise_d_matrix = torch.transpose(pairwise_d_matrix, dim0=0, dim1=1)
    pairwise_d_matrix[triu_indices[0], triu_indices[1]] = euclidean_dists

    return pairwise_d_matrix


def get_label_weights(labels_list, device):
    labels_counter = collections.Counter(labels_list)
    labels_sum = sum(labels_counter.values())
    labels_weights = [labels_counter[label] for label in labels_list]
    labels_weights = [labels_sum / label_weight for label_weight in labels_weights]
    labels_weights = torch.tensor(labels_weights, device=device)
    labels_weights = labels_weights / labels_weights.sum()
    return labels_weights

# calculate pairwise kernel distance and bandwidth
def kernal_dist(x, h=-1):
    x_numpy = x.cpu().data.numpy()
    init_dist = pdist(x_numpy)
    pairwise_dists = squareform(init_dist)

    if h < 0:  # if h < 0, using median trick
        h = np.median(pairwise_dists)
        h = 0.05 * h ** 2 / np.log(x.shape[0] + 1)

    if x_numpy.shape[0] > 1:
        kernal_xj_xi = torch.exp(- torch.tensor(pairwise_dists) ** 2 / h)
    else:
        kernal_xj_xi = torch.tensor([[1]])
        h = np.float64(1)
    return kernal_xj_xi.to(x), h


def WGF_step(z_gen, target_grad):
    kernal_xj_xi, h = kernal_dist(z_gen, h=-1)
    kernal_xj_xi, h = kernal_xj_xi.float(), h.astype(float)
    d_kernal_xi = torch.zeros(z_gen.size()).to(z_gen)
    F1_d_kernal_xi = torch.zeros(z_gen.size()).to(z_gen)
    F2_d_kernal_xi = torch.zeros(z_gen.size()).to(z_gen)
    x = z_gen
    part_kernel = torch.sum(kernal_xj_xi, dim=1).unsqueeze(1)
    for i_index in range(x.size()[0]):
        quot_ele = torch.div(x[i_index] - x, part_kernel)
        F1_d_kernal_xi[i_index] = torch.matmul(kernal_xj_xi[i_index], quot_ele) * 2 / h
        F2_d_kernal_xi[i_index] = torch.matmul(kernal_xj_xi[i_index], x[i_index] - x) / (
            torch.sum(kernal_xj_xi[i_index])) * 2 / h
        d_kernal_xi[i_index] = torch.matmul(kernal_xj_xi[i_index], x[i_index] - x) * 2 / h

    current_grad = (torch.matmul(kernal_xj_xi, target_grad) + d_kernal_xi)
    return current_grad


