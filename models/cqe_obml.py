import logging
import time

import torch
from torch import nn

import numpy as np

from torch.utils import data
from transformers import AdamW

import datasets
import models.utils
from models.base_models import Transformer,Linear,ReplayMemory

from models.utils import WGF_step
from models.metrics import confusion_matrix

logging.basicConfig(level='INFO', format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')
logger = logging.getLogger('CQE-OBML-Log')


class CQE_OBML:
    def __init__(self, device, n_classes, **kwargs):
        self.inner_lr = kwargs.get('inner_lr')
        self.meta_lr = kwargs.get('meta_lr')
        self.write_prob = kwargs.get('write_prob')
        self.replay_rate = kwargs.get('replay_rate')
        self.replay_every = kwargs.get('replay_every')
        num_particles = kwargs.get('num_particles')
        self.device = device

        logger.addHandler(kwargs.get('log_handler'))
        self.transformer = Transformer(model_name=kwargs.get('model'),
                                       max_length=kwargs.get('max_length'),
                                       device=device)

        self.linear = Linear(in_dim=768, out_dim=n_classes, num_particles=num_particles, device=device,
                             lmbd=kwargs.get('lmbda'))
        self.memory = ReplayMemory(write_prob=self.write_prob, tuple_size=2)

        meta_params = [p for p in self.transformer.parameters() if p.requires_grad] + [self.linear.alpha_lr]

        self.meta_optimizer = AdamW(meta_params, lr=self.meta_lr)

    def evaluate(self, dataloader):
        all_losses, all_predictions, all_labels = [], [], []
        for text, labels in dataloader:
            labels = torch.tensor(labels).to(self.device)
            input_dict = self.transformer.encode_text(text)
            with torch.no_grad():
                repr = self.transformer(input_dict)
                output, loss = self.linear(repr, self.linear.theta, labels)
            loss = loss.item()

            pred = models.utils.make_prediction(output.detach())
            all_losses.append(loss)
            all_predictions.extend(pred.tolist())
            all_labels.extend(labels.tolist())

        acc, prec, rec, f1 = models.utils.calculate_metrics(all_predictions, all_labels)
        logger.info('Test metrics: Loss = {:.4f}, ACCURACY = {:.4f}, PRECISION = {:.4f}, RECALL = {:.4f}, '
                    'F1 = {:.4f}'.format( np.mean(all_losses), acc, prec, rec, f1))

        torch.cuda.empty_cache()

        return acc, prec, rec, f1

    def train(self, train_datasets, dev_datasets, test_datasets, **kwargs):
        time_start = time.time()

        updates = kwargs.get('updates')
        mini_batch_size = kwargs.get('mini_batch_size')
        epochs = kwargs.get('n_epochs')

        replay_freq = self.replay_every // mini_batch_size
        replay_steps = int(self.replay_every * self.replay_rate / mini_batch_size)

        logger.info('Replay frequency: {}'.format(replay_freq))
        logger.info('Replay steps: {}'.format(replay_steps))

        final_pearsons, final_spearmans, final_rmses, final_maes, final_tids = [], [], [], [], []
        accuracies, precisions, recalls, f1s = self.test(test_datasets, **kwargs)
        final_pearsons.append(accuracies)
        final_spearmans.append(precisions)
        final_rmses.append(recalls)
        final_maes.append(f1s)
        final_tids.append(0)

        for index, (train_dataset, dev_dataset) in enumerate(zip(train_datasets, dev_datasets)):

                for epoch in range(epochs):
                    train_dataloader = iter(data.DataLoader(train_dataset, batch_size=mini_batch_size, shuffle=True,
                                                            collate_fn=datasets.utils.batch_encode))

                    num_batches = len(train_dataloader)
                    iteration = 0
                    while iteration < num_batches:
                        support_set = []
                        fast_weights = None

                        support_text, support_labels = next(train_dataloader)
                        support_set.append((support_text, support_labels))
                        self.memory.write_batch(support_text,support_labels)

                        for i in range(updates):

                            meta_update_set = []
                            query_text, query_labels = self.memory.read_batch(batch_size=mini_batch_size)

                            meta_update_set.append((query_text,query_labels))
                            meta_update_set.append((support_text, support_labels))

                            labels = torch.tensor(support_labels).to(self.device)
                            input_dict = self.transformer.encode_text(support_text)
                            repr = self.transformer(input_dict)
                            output, loss, fast_weights = self.inner_update(repr, fast_weights, labels)

                            pred_list = []
                            labels_list = []
                            query_loss = []

                            for (text, labels) in meta_update_set:
                                labels = torch.tensor(labels).to(self.device)
                                input_dict = self.transformer.encode_text(text)
                                repr = self.transformer(input_dict)

                                output, loss = self.meta_loss(repr,fast_weights,labels)

                                query_loss.append(loss.item())
                                pred = models.utils.make_prediction(output.detach())

                                pred_list.extend(pred.tolist())
                                labels_list.extend(labels.tolist())

                                transformer_params = [p for p in self.transformer.parameters() if p.requires_grad]
                                transformer_grads = torch.autograd.grad(loss, transformer_params, retain_graph=True)
                                for param, meta_grad in zip(transformer_params, transformer_grads):
                                    if param.grad is not None:
                                        param.grad += meta_grad.detach()
                                    else:
                                        param.grad = meta_grad.detach()

                                alpha_params = [self.linear.alpha_lr]
                                alpha_grads = torch.autograd.grad(loss, alpha_params, retain_graph=True)
                                for param, meta_grad in zip(alpha_params, alpha_grads):
                                    if param.grad is not None:
                                        param.grad += meta_grad.detach()
                                    else:
                                        param.grad = meta_grad.detach()


                                theta_params = [self.linear.theta]
                                theta_grads = torch.autograd.grad(loss, theta_params, retain_graph=True)
                                for param, meta_grad in zip(theta_params, theta_grads):
                                    if param.grad is not None:
                                        param.grad += meta_grad.detach()
                                    else:
                                        param.grad = meta_grad.detach()

                            self.meta_optimizer.step()
                            self.meta_optimizer.zero_grad()

                            self.linear.theta.data = self.linear.theta.data - self.linear.theta.grad * nn.functional.relu(self.linear.alpha_lr.data)

                        acc, prec, rec, f1 = models.utils.calculate_metrics(pred_list, labels_list)

                        logger.info('Epoch {}, task {}, query set: Loss = {:.4f}, ACCURACY = {:.4f}, PRECISION = {'
                                    ':.4f}, RECALL = {:.4f}, F1 = {:.4f}'.format(epoch + 1,train_dataset.task_name,
                                                                                 np.mean(query_loss), acc, prec, rec,
                                                                                 f1))

                        torch.cuda.empty_cache()

                        iteration += 1

                accuracies, precisions, recalls, f1s = self.test(test_datasets, **kwargs)
                final_pearsons.append(accuracies)
                final_spearmans.append(precisions)
                final_rmses.append(recalls)
                final_maes.append(f1s)
                final_tids.append(index)

        all_results = [final_pearsons,final_spearmans,final_rmses, final_maes]
        all_results_names = ["Accuracy", "Precision", "Recall", "F1"]
        all_final_tids = [final_tids] * len(all_results)
        time_spent = time.time() - time_start
        self.save_results(all_results,all_results_names,all_final_tids,time_spent, **kwargs)

    def save_results(self, all_results,all_results_names,all_final_tids,time_spent, **kwargs):
        run_dir = kwargs.get("run_dir")

        with open(run_dir+'/'+'other_results.txt',mode="w") as out_fh:
            one_liner = ' # test: '
            for (result_test_t, result_test_a, result_name) in zip(all_final_tids, all_results, all_results_names):
                result_test_t, result_test_a = torch.Tensor(result_test_t), torch.Tensor(result_test_a)
                test_stats = confusion_matrix(result_test_t, result_test_a, run_dir, result_name)
                one_liner += ' ' + result_name + '(Diagonal/Final/Backward/Forward)' + ' : ' + ' '.join(["%.3f" % stat
                                                                                                        for
                                                                                                 stat  in test_stats])

            print(one_liner + ' # Run time(secs): ' + str(time_spent), file=out_fh, flush=True)


    def inner_update(self, x, fast_weights, y):
        if fast_weights is None:
            fast_weights = self.linear.theta

        logits, loss = self.linear(x, fast_weights, y)
        graph_required = True

        grads = torch.autograd.grad(loss, fast_weights, retain_graph=graph_required)[0]
        phi = WGF_step(fast_weights, grads)
        fast_weights = fast_weights + phi * nn.functional.relu(self.linear.alpha_lr)

        return logits, loss, fast_weights


    def meta_loss(self, x, fast_weights, y):
        logits, loss_q = self.linear.forward(x, fast_weights, y)
        return logits, loss_q

    def test(self, test_datasets, **kwargs):
        updates = kwargs.get('updates')
        mini_batch_size = kwargs.get('mini_batch_size')

        accuracies, precisions, recalls, f1s = [], [], [], []
        for test_dataset in test_datasets:
            logger.info('Testing on {}'.format(test_dataset.dataset_name))
            test_dataloader = data.DataLoader(test_dataset, batch_size=mini_batch_size, shuffle=False,
                                              collate_fn=datasets.utils.batch_encode)

            acc, prec, rec, f1 = self.evaluate(dataloader=test_dataloader)
            accuracies.append(acc)
            precisions.append(prec)
            recalls.append(rec)
            f1s.append(f1)

        logger.info('Overall test metrics: ACCURACY = {:.4f}, PRECISION = {:.4f}, RECALL = {:.4f}, F1 = {:.4f}'.format(np.mean(accuracies), np.mean(precisions), np.mean(recalls),np.mean(f1s)))

        return accuracies, precisions, recalls, f1s
