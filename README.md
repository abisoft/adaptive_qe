



Code that implements the approach described in the paper [1], adapted to work with Bergamot data. Check the `matrix_bmaml` branch for the approach described in the paper [2] that also works with Bergamot data.

Tested with python 3.8, pytorch 1.4 and transformers 3.0

To run, first download the data used from [here](https://drive.google.com/drive/folders/1f7Fy-SpAcNIFRiMEtWSG4nY2hUlkZlih?usp=sharing), and place it into the data directory.

Then run with the command: `python train.py`


Acknowledgements: Thanks to the authors of [transformers](https://github.com/huggingface/transformers), [metalifelonglanguage](https://github.com/Nithin-Holla/MetaLifelongLanguage), [few-shot meta-learning](https://github.com/cnguyen10/few_shot_meta_learning), [La-MAML](https://github.com/montrealrobotics/La-MAML), [meta-sampling](https://github.com/zheshiyige/meta-sampling), [matrix_svgd](https://github.com/dilinwang820/matrix_svgd) and [hessian](https://github.com/mariogeiger/hessian) for their awesome open source work.


References:

[1] Abiola Obamuyide, Marina Fomicheva and Lucia Specia (2021). Continual Quality Estimation with Online Bayesian Meta-Learning. Proceedings of the 59th Annual Meeting of the Association for Computational Linguistics and the 11th International Joint Conference on Natural Language Processing.

[2] Abiola Obamuyide, Marina Fomicheva and Lucia Specia (2021). Bayesian Model-Agnostic Meta-Learning with Matrix-Valued Kernels for Quality Estimation. Proceedings of the 6th Workshop on Representation Learning for NLP (RepL4NLP-2021).









