import logging
import os
import random, datetime
from argparse import ArgumentParser


import numpy as np
import torch
import datasets.utils
from models.cqe_obml import CQE_OBML

logging.basicConfig(level='INFO', format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')
logger = logging.getLogger('ContinualLearningLog')


if __name__ == '__main__':

    DATASET_ORDER_MAPPINGS = {
        "wptp2012_dataset":{
            1: ['A1','A2','A3','A4','A5','A6','A7','A8'],
            2: ['A2', 'A5', 'A8', 'A1', 'A6', 'A3', 'A7', 'A4'],
            3: ['A4', 'A6', 'A2', 'A8', 'A1', 'A3', 'A7', 'A5'],
            4: ['A7', 'A4', 'A6', 'A3', 'A5', 'A1', 'A8', 'A2'],
            5: ['A5', 'A8', 'A6', 'A1', 'A2', 'A7', 'A3', 'A4'],
        },
        "en-lv.nmt": {
            1: ['PE8', 'PE2', 'PE3', 'PE5', 'PE4','PE6', 'PE7'],
            2: ['PE2', 'PE5', 'PE7', 'PE6', 'PE3', 'PE4', 'PE8'],
            3: ['PE5', 'PE4', 'PE6', 'PE8', 'PE7', 'PE2', 'PE3'],
            4: ['PE2', 'PE8', 'PE6', 'PE3', 'PE4', 'PE7', 'PE5'],
            5: ['PE4', 'PE2', 'PE5', 'PE3', 'PE6', 'PE8', 'PE7'],
        },
        "en-cs.smt": {
            1: ['PE2', 'PE5','PE3', 'PE1', 'PE4'],
            2: ['PE4', 'PE2', 'PE5', 'PE3', 'PE1'],
            3: ['PE2', 'PE3', 'PE5', 'PE1', 'PE4'],
            4: ['PE1', 'PE5', 'PE3', 'PE4', 'PE2'],
            5: ['PE2', 'PE5', 'PE1', 'PE4', 'PE3'],
        },
        "bergamot_mlqe/en-et": {
            1: ['ann0', 'ann1', 'ann2'],
            2: ['ann1', 'ann2', 'ann0'],
            3: ['ann2', 'ann1', 'ann0'],
        },
        "bergamot_mlqe/en-es": {
            1: ['ann0', 'ann1', 'ann2'],
            2: ['ann1', 'ann2', 'ann0'],
            3: ['ann2', 'ann1', 'ann0'],
        },
        "bergamot_mlqe/en-cs": {
            1: ['ann54', 'ann55', 'ann56', 'ann57', 'ann58', 'ann59'],
            2: ['ann55', 'ann56', 'ann57', 'ann54', 'ann58', 'ann59'],
            3: ['ann56', 'ann57', 'ann58', 'ann54', 'ann55', 'ann59'],
        }
    }

    n_classes = 1

    # Parse command line arguments
    parser = ArgumentParser()
    parser.add_argument('--order', type=int, help='Order of datasets', default=1)

    parser.add_argument('--n_epochs', type=int, help='Number of epochs (only for MTL)', default=1) #15

    parser.add_argument('--n_meta_epochs', type=int, help='Number of meta epochs', default=1)
    parser.add_argument('--meta_batch_size', type=int, help='Meta batch size of tasks',default=3)

    parser.add_argument('--num_particles', type=int, help='Number of particles', default=3)
    parser.add_argument('--learner', type=str, help='Learner method', default='cqe_obml')

    parser.add_argument('--seed', type=int, help='Random seed', default=42)

    parser.add_argument('--dataset', type=str, help='dataset', default='bergamot_mlqe/en-et')

    parser.add_argument('--num_train_instances', type=int, help='Number of training instances (-1 = all)', default=100)

    parser.add_argument('--lr', type=float, help='Learning rate (only for the baselines)', default=3e-5)
    parser.add_argument('--inner_lr', type=float, help='Inner-loop learning rate', default=3e-5)
    parser.add_argument('--updates', type=int, help='Number of inner-loop updates', default=1) # 15, set to 2 for full
    #
    parser.add_argument('--meta_lr', type=float, help='Meta learning rate', default=3e-5)

    parser.add_argument('--model', type=str, help='Name of the model', default='distil-multi-bert')
    parser.add_argument('--mini_batch_size', type=int, help='Batch size of data points within an episode', default=16)
    parser.add_argument('--write_prob', type=float, help='Write probability for buffer memory', default=1.0)
    parser.add_argument('--max_length', type=int, help='Maximum sequence length for the input', default=100)
    parser.add_argument('--replay_rate', type=float, help='Replay rate from memory', default=1.0)
    parser.add_argument('--replay_every', type=int, help='Number of data points between replay', default=16)

    args = parser.parse_args()


    run_dir = "data" + "/bergamot_metaclexps_classification/runs_order_"+str(args.order)+"/"+str(
            args.seed)+"/"+args.dataset+"/"+ args.learner + \
                  "/" +str(args.num_particles) + "/"+str(args.num_train_instances)+ "/"+datetime.datetime.now().strftime('%Y-%m-%d_%H-%M-%S')

    os.makedirs(run_dir, exist_ok=True)

    handler = logging.StreamHandler(open(run_dir+"/"+"run.txt", 'w', encoding="utf-8"))
    handler.setLevel(logging.INFO)
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    handler.setFormatter(formatter)
    args.log_handler = handler
    args.run_dir = run_dir


    logger.addHandler(handler)
    logger.info('Using configuration: {}'.format(vars(args)))

    # Set base path
    base_path = os.path.dirname(os.path.abspath(__file__))
    base_path = base_path.replace("\\","/")

    torch.manual_seed(args.seed)
    random.seed(args.seed)
    np.random.seed(args.seed)

    logger.info('Loading the datasets')
    train_datasets, dev_datasets, test_datasets = [], [], []
    dataset_order_mapping = DATASET_ORDER_MAPPINGS[args.dataset]

    for dataset_id in dataset_order_mapping[args.order]:
        train_dataset, dev_dataset, test_dataset = datasets.utils.get_dataset(base_path, args.dataset, dataset_id,
                                                                              num_train_instances=args.num_train_instances)
        logger.info('Loaded {}'.format(train_dataset.dataset_name))
        train_datasets.append(train_dataset)
        dev_datasets.append(dev_dataset)
        test_datasets.append(test_dataset)
    logger.info('Finished loading all datasets')

    # Load the model
    device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
    learner = CQE_OBML(device=device, n_classes=n_classes, **vars(args))
    logger.info('Using {} as learner'.format(learner.__class__.__name__))

    # Training
    model_file_name = learner.__class__.__name__ + '-' + str(datetime.datetime.now()).replace(':', '-').replace(' ',
                                                                                                        '_') + '.pt'
    model_dir = os.path.join(base_path,'saved_models')
    os.makedirs(model_dir, exist_ok=True)

    logger.info('----------Training starts here----------')
    learner.train(train_datasets, dev_datasets, test_datasets, **vars(args))


